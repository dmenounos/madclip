# create user 'madclip'@'localhost' identified by 'qweasd';
# grant select, insert, update, delete on madclip.* to 'madclip'@'localhost';
# grant select on mcrealm.* to 'madclip'@'localhost';

drop database if exists mcrealm;
create database mcrealm;
use mcrealm;

create table User (
	username            varchar(15) not null primary key,
	password            varchar(15) not null
);

create table Role (
	username            varchar(15) not null,
	rolename            varchar(15) not null,
	primary key (username, rolename),
	foreign key (username) references User(username)
);

drop database if exists madclip;
create database madclip;
use madclip;

create table Ad (
	id                  int not null auto_increment primary key,
	issue               int not null,       -- τεύχος
	page                int not null,       -- αριθμός σελίδας
	baseType            int not null,       -- βασικός τύπος
	exactType           int,                -- ακριβής τύπος
	position            int,                -- διακεκριμένη θέση
	affix               boolean,            -- επικόλληση
	reportage           int,                -- reportage
	totalLook           boolean,            -- total look
	size                int,                -- μέγεθος
	category            int,                -- κατηγορία / είδος
	subCategory         int,                -- υποείδος
	trademark           int,                -- trademark
	brandname           int,                -- brandname
	advertiser          int,                -- διαφημιζόμενος
	shop                int,                -- shop
	comments            varchar(255),       -- παρατηρήσεις
	supCategory         varchar(255),       -- βοηθητική κατηγορία / είδος
	supSubCategory      varchar(255),       -- βοηθητικό υποείδος
	supTrademark        varchar(255),       -- βοηθητικό trademark
	supBrandname        varchar(255),       -- βοηθητικό brandname
	supAdvertiser       varchar(255),       -- βοηθητικό διαφημιζόμενος
	supShop             varchar(255),       -- βοηθητικό shop
	supComments         varchar(255)        -- βοηθητικά σχόλια
) engine=InnoDB;

create table AdSize (
	id                  int not null auto_increment primary key,
	size                int not null,
	name                varchar(255) not null
) engine=InnoDB;

create table AdType (
	id                  int not null auto_increment primary key,
	baseType            int not null,
	description         varchar(255) not null
) engine=InnoDB;

create table Publisher (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null
) engine=InnoDB;

create table Magazine (
	id                  int not null auto_increment primary key,
	title               varchar(255) not null,
	publisher           int not null
) engine=InnoDB;

create table Extra (
	id                  int not null auto_increment primary key,
	title               varchar(255) not null,
	magazine            int not null
) engine=InnoDB;

create table Issue (
	id                  int not null auto_increment primary key,
	publisher           int not null,
	magazine            int not null,
	extra               int,
	period              date not null,
	label               varchar(255) not null,
	pages               int not null
) engine=InnoDB;

create table DiscretePosition (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null
) engine=InnoDB;

create table Reportage (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null,
	magazine            int not null,
	extra               int
) engine=InnoDB;

create table Category (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null,
	category            int
) engine=InnoDB;

create table SubCategory (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null,
	category            int not null
) engine=InnoDB;

create table Trademark (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null
) engine=InnoDB;

create table TrademarkCategory (
	id                  int not null auto_increment primary key,
	trademark           int not null,
	category            int not null
) engine=InnoDB;

create table Brandname (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null,
	trademark           int not null
) engine=InnoDB;

create table Advertiser (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null
) engine=InnoDB;

create table AdvertiserTrademark (
	id                  int not null auto_increment primary key,
	advertiser          int not null,
	trademark           int not null
) engine=InnoDB;

create table Shop (
	id                  int not null auto_increment primary key,
	name                varchar(255) not null,
	advertiser          int not null
) engine=InnoDB;

create table Audit (
	entityClass         varchar(255) not null,
	entityId            int not null,
	createUser          varchar(255) not null,
	createDate          timestamp not null default 0,
	updateUser          varchar(255),
	updateDate          timestamp null default null,
	primary key (entityClass, entityId)
) engine=InnoDB;

alter table Ad add foreign key (issue) references Issue(id);
alter table Ad add foreign key (exactType) references AdType(id);
alter table Ad add foreign key (position) references DiscretePosition(id);
alter table Ad add foreign key (reportage) references Reportage(id);
alter table Ad add foreign key (size) references AdSize(id);
alter table Ad add foreign key (category) references Category(id);
alter table Ad add foreign key (subCategory) references SubCategory(id);
alter table Ad add foreign key (trademark) references Trademark(id);
alter table Ad add foreign key (brandname) references Brandname(id);
alter table Ad add foreign key (advertiser) references Advertiser(id);
alter table Ad add foreign key (shop) references Shop(id);

alter table AdSize add unique index (size);

alter table AdType add unique index (baseType, description);

alter table Publisher add unique index (name);

alter table Magazine add foreign key (publisher) references Publisher(id);
alter table Magazine add unique index (title, publisher);

alter table Extra add foreign key (magazine) references Magazine(id);
alter table Extra add unique index (title, magazine);

alter table Issue add foreign key (publisher) references Publisher(id);
alter table Issue add foreign key (magazine) references Magazine(id);
alter table Issue add foreign key (extra) references Extra(id);

alter table DiscretePosition add unique index (name);

alter table Reportage add foreign key (magazine) references Magazine(id);
alter table Reportage add unique index (name, magazine);

alter table Category add foreign key (category) references Category(id);
alter table Category add unique index (name, category);

alter table SubCategory add foreign key (category) references Category(id);
alter table SubCategory add unique index (name, category);

alter table Trademark add unique index (name);

alter table TrademarkCategory add foreign key (trademark) references Trademark(id);
alter table TrademarkCategory add foreign key (category) references Category(id);
alter table TrademarkCategory add unique index (trademark, category);

alter table Brandname add foreign key (trademark) references Trademark(id);
alter table Brandname add unique index (name, trademark);

alter table Advertiser add unique index (name);

alter table AdvertiserTrademark add foreign key (advertiser) references Advertiser(id);
alter table AdvertiserTrademark add foreign key (trademark) references Trademark(id);
alter table AdvertiserTrademark add unique index (advertiser, trademark);

alter table Shop add foreign key (advertiser) references Advertiser(id);
alter table Shop add unique index (name, advertiser);
