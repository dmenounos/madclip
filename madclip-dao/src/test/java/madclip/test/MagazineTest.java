/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.test;

import org.springframework.context.ApplicationContext;

import madclip.model.Magazine;
import madclip.model.Publisher;
import madclip.util.SpringUtils;

import mojo.dao.DataService;
import mojo.dao.spec.ById;
import mojo.dao.spec.Select;

public class MagazineTest extends BaseTest {

	private DataService<Publisher> publisherService;
	private DataService<Magazine> magazineService;

	private Publisher publisherA;
	private Publisher publisherB;

	@SuppressWarnings("unchecked")
	public MagazineTest(String testName) {
		super(testName);

		ApplicationContext appContext = SpringUtils.getApplicationContext();
		publisherService = (DataService<Publisher>) appContext.getBean("publisherService");
		magazineService = (DataService<Magazine>) appContext.getBean("publisherMagazinesService");
	}

	@Override
	protected void setUp() {
		super.setUp();

		log("Creating publisher");
		publisherA = new Publisher();
		publisherA.setName("Δημοσιογραφικός Οργανισμός Λαμπράκη");
		publisherA = publisherService.insert(publisherA);

		log("Creating publisher");
		publisherB = new Publisher();
		publisherB.setName("Hachette Rizzoli");
		publisherB = publisherService.insert(publisherB);
	}

	@Override
	protected void tearDown() {
		log("Deleting publisher #" + publisherA.getId());
		publisherService.delete(publisherA.getId());

		log("Deleting publisher #" + publisherB.getId());
		publisherService.delete(publisherB.getId());

		super.tearDown();
	}

	public void testCRUD() {
		log("Creating magazine");
		Magazine magazine = new Magazine();
		magazine.setTitle("Elle");
		magazine.setPublisher(publisherA);
		magazine = magazineService.insert(magazine);
		assertValidMagazine(magazine);
		assertInsertAudit(getAudit(magazine));

		log("Retrieving magazine #" + magazine.getId());
		Magazine loadedMagazine = magazineService.select(new Select<Magazine>(Magazine.class, new ById(magazine.getId())).join("publisher")).unique();
		assertEqualMagazines(magazine, loadedMagazine);

		log("Modifying magazine #" + magazine.getId());
		magazine.setTitle("Marie Claire");
		magazine.setPublisher(publisherB);
		Magazine updatedMagazine = magazineService.update(magazine);
		assertValidMagazine(updatedMagazine);
		assertUpdateAudit(getAudit(magazine));

		log("Retrieving magazine #" + magazine.getId());
		loadedMagazine = magazineService.select(new Select<Magazine>(Magazine.class, new ById(magazine.getId())).join("publisher")).unique();
		assertEqualMagazines(magazine, loadedMagazine);

		log("Deleting magazine #" + magazine.getId());
		magazineService.delete(magazine.getId());
		magazine = magazineService.findById(magazine.getId());
		assertNull("magazine should be null", magazine);
	}

	protected static void assertValidMagazine(Magazine magazine) {
		assertNotNull("null magazine", magazine);
		assertNotNull("null magazine.id", magazine.getId());
		PublisherTest.assertValidPublisher(magazine.getPublisher());
	}

	protected static void assertEqualMagazines(Magazine exp, Magazine act) {
		if (exp == null) {
			assertNull("not null magazine", act);
		}
		else {
			assertNotNull("null magazine", act);
			assertEquals("incorrect magazine.id", exp.getId(), act.getId());
			assertEquals("incorrect magazine.title", exp.getTitle(), act.getTitle());
			PublisherTest.assertEqualPublishers(exp.getPublisher(), act.getPublisher());
		}
	}
}
