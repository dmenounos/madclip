/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.test;

import junit.framework.TestCase;

import org.springframework.context.ApplicationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import madclip.model.AbstractEntity;
import madclip.model.Audit;
import madclip.util.SpringUtils;

import mojo.dao.Repository;
import mojo.dao.spec.ById;
import mojo.dao.spec.Select;

public abstract class BaseTest extends TestCase {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	protected Repository<Audit> auditRepository;

	@SuppressWarnings("unchecked")
	protected BaseTest(String name) {
		super(name);

		ApplicationContext appContext = SpringUtils.getApplicationContext();
		auditRepository = (Repository<Audit>) appContext.getBean("auditRepository");
	}

	@Override
	protected void setUp() {
		logger.debug("--> " + getName());
	}

	@Override
	protected void tearDown() {
		logger.debug("<-- " + getName());
	}

	protected void log(String msg) {
		logger.debug("### " + msg);
	}

	protected void assertInsertAudit(Audit audit) {
		assertNotNull("null audit", audit);
		assertNotNull("null audit.createUser", audit.getCreateUser());
		assertNotNull("null audit.createDate", audit.getCreateDate());
	}

	protected void assertUpdateAudit(Audit audit) {
		assertNotNull("null audit", audit);
		assertNotNull("null audit.updateUser", audit.getUpdateUser());
		assertNotNull("null audit.updateDate", audit.getUpdateDate());
	}

	protected Audit getAudit(AbstractEntity entity) {
		Audit.Id id = new Audit.Id();
		id.setEntityClass(entity.getClass().getName());
		id.setEntityId(entity.getId());

		ById byid = new ById(id);
		Select<Audit> select = new Select<Audit>(Audit.class, byid);
		return auditRepository.execute(select).unique();
	}
}
