/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.test;

import org.springframework.context.ApplicationContext;

import madclip.model.Publisher;
import madclip.util.SpringUtils;

import mojo.dao.DataService;

public class PublisherTest extends BaseTest {

	private DataService<Publisher> publisherService;

	@SuppressWarnings("unchecked")
	public PublisherTest(String testName) {
		super(testName);

		ApplicationContext appContext = SpringUtils.getApplicationContext();
		publisherService = (DataService<Publisher>) appContext.getBean("publisherService");
	}

	public void testCRUD() {
		log("Creating publisher");
		Publisher publisher = new Publisher();
		publisher.setName("Δημοσιογραφικός Οργανισμός Λαμπράκη");
		publisher = publisherService.insert(publisher);
		assertValidPublisher(publisher);
		assertInsertAudit(getAudit(publisher));

		log("Retrieving publisher #" + publisher.getId());
		Publisher loadedPublisher = publisherService.findById(publisher.getId());
		assertEqualPublishers(publisher, loadedPublisher);

		log("Modifying publisher #" + publisher.getId());
		publisher.setName("Hachette Rizzoli");
		Publisher updatedPublisher = publisherService.update(publisher);
		assertValidPublisher(updatedPublisher);
		assertUpdateAudit(getAudit(publisher));

		log("Retrieving publisher #" + publisher.getId());
		loadedPublisher = publisherService.findById(publisher.getId());
		assertEqualPublishers(publisher, loadedPublisher);

		log("Deleting publisher #" + publisher.getId());
		publisherService.delete(publisher.getId());
		publisher = publisherService.findById(publisher.getId());
		assertNull("publisher should be null", publisher);
	}

	protected static void assertValidPublisher(Publisher publisher) {
		assertNotNull("null publisher", publisher);
		assertNotNull("null publisher.id", publisher.getId());
	}

	protected static void assertEqualPublishers(Publisher exp, Publisher act) {
		if (exp == null) {
			assertNull("not null publisher", act);
		}
		else {
			assertNotNull("null publisher", act);
			assertEquals("incorect publisher.id", exp.getId(), act.getId());
			assertEquals("incorrect publisher.name", exp.getName(), act.getName());
		}
	}
}
