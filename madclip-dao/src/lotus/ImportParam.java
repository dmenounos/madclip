/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.sql.Connection;
import java.sql.DriverManager;

import lotus.domino.Session;
import lotus.domino.Database;
import lotus.domino.NotesThread;
import lotus.domino.NotesFactory;

import rep.PublisherRepository;
import rep.MagazineRepository;
import rep.ExtraRepository;
import rep.SizeRepository;
import rep.PositionRepository;
import rep.CategoryRepository;
import rep.TrademarkRepository;
import rep.BrandnameRepository;
import rep.AdvertiserRepository;
import rep.ShopRepository;

import imp.OmiloiPeriodika;
import imp.Extras;
import imp.Megethi;
import imp.Positions;
import imp.Categories;
import imp.SubCategories1;
import imp.SubCategories2;
import imp.Trademarks;
import imp.Brandnames;
import imp.Advertisers;
import imp.Shops;

public class ImportParam {

	public static void main(String[] args) throws Exception {
		if (args.length != 2) throw new RuntimeException("nsf path and ip address needed");
		
		NotesThread.sinitThread();
		
		Session ss = null;
		Database db = null;
		
		Connection conn = null;
		
		try {
			// setup resources
			
			ss = NotesFactory.createSession();
			db = ss.getDatabase(null, args[0]);
			
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://" + args[1] + ":3306/madclip", "mcadmin", "qweasd");
			
			// setup repositories
			
			PublisherRepository publisherRepository = new PublisherRepository();
			publisherRepository.connection = conn;
			
			MagazineRepository magazineRepository = new MagazineRepository();
			magazineRepository.connection = conn;
			
			ExtraRepository extraRepository = new ExtraRepository();
			extraRepository.connection = conn;
			
			SizeRepository sizeRepository = new SizeRepository();
			sizeRepository.connection = conn;
			
			PositionRepository positionRepository = new PositionRepository();
			positionRepository.connection = conn;
			
			CategoryRepository categoryRepository = new CategoryRepository();
			categoryRepository.connection = conn;
			
			TrademarkRepository trademarkRepository = new TrademarkRepository();
			trademarkRepository.connection = conn;
			
			BrandnameRepository brandnameRepository = new BrandnameRepository();
			brandnameRepository.connection = conn;
			
			AdvertiserRepository advertiserRepository = new AdvertiserRepository();
			advertiserRepository.connection = conn;
			
			ShopRepository shopRepository = new ShopRepository();
			shopRepository.connection = conn;
			
			// setup services
			
			OmiloiPeriodika omiloiPeriodika = new OmiloiPeriodika();
			omiloiPeriodika.db = db;
			omiloiPeriodika.publisherRepository = publisherRepository;
			omiloiPeriodika.magazineRepository = magazineRepository;
			omiloiPeriodika.exec();
			
			Extras extras = new Extras();
			extras.db = db;
			extras.magazineRepository = magazineRepository;
			extras.extraRepository = extraRepository;
			extras.exec();
			
			Megethi megethi = new Megethi();
			megethi.db = db;
			megethi.sizeRepository = sizeRepository;
			megethi.exec();
			
			Positions positions = new Positions();
			positions.db = db;
			positions.positionRepository = positionRepository;
			positions.exec();
			
			Categories categories = new Categories();
			categories.db = db;
			categories.categoryRepository = categoryRepository;
			categories.exec();
			
			SubCategories1 subCategories1 = new SubCategories1();
			subCategories1.db = db;
			subCategories1.categoryRepository = categoryRepository;
			subCategories1.exec();
			
			SubCategories2 subCategories2 = new SubCategories2();
			subCategories2.db = db;
			subCategories2.categoryRepository = categoryRepository;
			subCategories2.exec();
			
			Trademarks trademarks = new Trademarks();
			trademarks.db = db;
			trademarks.categoryRepository = categoryRepository;
			trademarks.trademarkRepository = trademarkRepository;
			trademarks.exec();
			
			Brandnames brandnames = new Brandnames();
			brandnames.db = db;
			brandnames.trademarkRepository = trademarkRepository;
			brandnames.brandnameRepository = brandnameRepository;
			brandnames.exec();
			
			Advertisers advertisers = new Advertisers();
			advertisers.db = db;
			advertisers.trademarkRepository = trademarkRepository;
			advertisers.advertiserRepository = advertiserRepository;
			advertisers.exec();
			
			Shops shops = new Shops();
			shops.db = db;
			shops.advertiserRepository = advertiserRepository;
			shops.shopRepository = shopRepository;
			shops.exec();
		} finally {
			if (conn != null) 
				conn.close();
				
			if (db != null) 
				db.recycle();
				
			if (ss != null) 
				ss.recycle();
		}
		
		NotesThread.stermThread();
	}
}
