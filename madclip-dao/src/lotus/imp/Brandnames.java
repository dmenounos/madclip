/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package imp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lotus.domino.View;
import lotus.domino.Document;
import lotus.domino.Database;

import rep.TrademarkRepository;
import rep.BrandnameRepository;

public class Brandnames extends Service {

	public TrademarkRepository trademarkRepository;
	public BrandnameRepository brandnameRepository;

	public void exec() throws Exception {
		logger.info("Importing brandnames");
		
		Map<String, Integer> trademarks = new HashMap<String, Integer>();
		Set<IdValue> tuples = new HashSet<IdValue>();
		
		View vw = null;
		
		try {
			vw = db.getView("TrademBrandVW");
			
			for (Document doc = vw.getFirstDocument(); doc != null; doc = vw.getNextDocument(doc)) {
				String brandname = strValue(doc, "SecBrandNameFLD");
				String trademark = strValue(doc, "Sec2TrademarkFLD");
				
				if (brandname == null || trademark == null) {
					logger.error("Aborting creation; Null value(s)");
					continue;
				}
				
				Integer trademarkId = trademarks.get(trademark);
				
				if (trademarkId == null) {
					trademarkId = trademarkRepository.findTrademarkId(trademark);
				}
				
				if (trademarkId == null) {
					logger.debug("Creating trademark: " + trademark);
					trademarkId = trademarkRepository.createTrademark(trademark);
				}
				
				trademarks.put(trademark, trademarkId);
				
				boolean isUnique = tuples.add(new IdValue(trademarkId, brandname));
				
				if (isUnique) {
					logger.debug("Creating brandname: " + brandname + " for trademark: " + trademark);
					brandnameRepository.createBrandname(trademarkId, brandname);
				}
			}
		} finally {
			if (vw != null)
				vw.recycle();
		}
	}
}
