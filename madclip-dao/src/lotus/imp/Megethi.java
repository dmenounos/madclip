/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package imp;

import java.util.HashMap;
import java.util.Map;

import lotus.domino.View;
import lotus.domino.Document;
import lotus.domino.Database;

import rep.SizeRepository;

public class Megethi extends Service {

	public SizeRepository sizeRepository;

	public void exec() throws Exception {
		logger.info("Importing sizes");
		
		Map<String, Integer> sizes = new HashMap<String, Integer>();
		int ainc = 0;
		
		View vw = null;
		
		try {
			vw = db.getView("MegethosVW");
			
			for (Document doc = vw.getFirstDocument(); doc != null; doc = vw.getNextDocument(doc)) {
				String size = strValue(doc, "SecMegethosFLD");
				
				if (size == null) {
					logger.error("Aborting creation; Null value(s)");
					continue;
				}
				
				Integer sizeId = sizes.get(size);
				
				if (sizeId == null) {
					logger.debug("Creating size: " + size);
					sizeId = sizeRepository.createSize(size, ++ainc);
					sizes.put(size, sizeId);
				}
			}
		} finally {
			if (vw != null)
				vw.recycle();
		}
	}
}
