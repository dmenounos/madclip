/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package imp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lotus.domino.View;
import lotus.domino.Document;
import lotus.domino.Database;

import rep.TrademarkRepository;
import rep.AdvertiserRepository;

public class Advertisers extends Service {

	public TrademarkRepository trademarkRepository;
	public AdvertiserRepository advertiserRepository;

	public void exec() throws Exception {
		logger.info("Importing advertisers");
		
		Map<String, Integer> trademarks = new HashMap<String, Integer>();
		Map<String, Integer> advertisers = new HashMap<String, Integer>();
		Set<IdValue> tuples = new HashSet<IdValue>();
		
		View vw = null;
		
		try {
			vw = db.getView("TrademAdvRepVW");
			
			for (Document doc = vw.getFirstDocument(); doc != null; doc = vw.getNextDocument(doc)) {
				String trademark = strValue(doc, "Sec3TrademarkFLD");
				String advertiser = strValue(doc, "SecAdvRepFLD");
				
				if (trademark == null || advertiser == null) {
					logger.error("Aborting creation; Null value(s)");
					continue;
				}
				
				Integer trademarkId = trademarks.get(trademark);
				
				if (trademarkId == null) {
					trademarkId = trademarkRepository.findTrademarkId(trademark);
				}
				
				if (trademarkId == null) {
					logger.debug("Creating trademark: " + trademark);
					trademarkId = trademarkRepository.createTrademark(trademark);
				}
				
				trademarks.put(trademark, trademarkId);
				
				Integer advertiserId = advertisers.get(advertiser);
				
				if (advertiserId == null) {
					logger.debug("Creating advertiser: " + advertiser);
					advertiserId = advertiserRepository.createAdvertiser(advertiser);
					advertisers.put(advertiser, advertiserId);
				}
				
				boolean isUnique = tuples.add(new IdValue(trademarkId, advertiser));
				
				if (isUnique) {
					logger.debug("Creating advertiser-traemark: " + advertiserId + "-" + trademarkId);
					advertiserRepository.createAdvertiserTrademark(advertiserId, trademarkId);
				}
			}
		} finally {
			if (vw != null)
				vw.recycle();
		}
	}
}
