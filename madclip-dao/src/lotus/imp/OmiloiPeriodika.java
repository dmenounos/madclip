/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package imp;

import java.util.HashMap;
import java.util.Map;

import lotus.domino.View;
import lotus.domino.Document;
import lotus.domino.Database;

import rep.PublisherRepository;
import rep.MagazineRepository;

public class OmiloiPeriodika extends Service {

	public PublisherRepository publisherRepository;
	public MagazineRepository magazineRepository;

	public void exec() throws Exception {
		logger.info("Importing publishers and magazines");
		
		View vw = null;
		
		try {
			vw = db.getView("PerOmilosVW");
			Document doc = vw.getFirstDocument();
			
			Map<String, Integer> publishers = new HashMap<String, Integer>();
			
			while (doc != null) {
				String magazine = strValue(doc, "SecPeriodFLD");
				String publisher = strValue(doc, "SecOmilosFLD");
				
				if (publisher.endsWith(" Α.Ε.")) {
					publisher = publisher.substring(0, publisher.indexOf(" Α.Ε."));
				}
				
				Integer publisherId = publishers.get(publisher);
				
				if (publisherId == null) {
					logger.debug("Creating publisher: " + publisher);
					publisherId = publisherRepository.createPublisher(publisher);
					publishers.put(publisher, publisherId);
				}
				
				if (!magazine.endsWith(" EXTRA")) {
					logger.debug("Creating magazine: " + magazine + " for publisher: " + publisher);
					magazineRepository.createMagazine(publisherId, magazine);
				}
				
				doc = vw.getNextDocument(doc);
			}
		} finally {
			if (vw != null)
				vw.recycle();
		}
	}
}
