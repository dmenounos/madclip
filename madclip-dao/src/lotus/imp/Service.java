/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package imp;

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lotus.domino.Database;
import lotus.domino.Document;

public abstract class Service {

	public Database db;
	public final Logger logger = LoggerFactory.getLogger(getClass());

	public abstract void exec() throws Exception;

	protected String strValue(Document doc, String item) throws Exception {
		String value = (String) value(doc, item);
		
		if (value != null) {
			value = value.trim().toUpperCase();
		}
		
		return value;
	}

	protected Object value(Document doc, String item) throws Exception {
		Vector vector = doc.getItemValue(item);
		
		if (vector != null && vector.size() == 1) {
			return vector.get(0);
		}
		
		return null;
	}
}
