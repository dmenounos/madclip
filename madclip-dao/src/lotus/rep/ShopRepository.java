/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package rep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ShopRepository extends Repository {

	public Integer createShop(Integer advertiserId, String name) {
		try {
			PreparedStatement statement = connection.prepareStatement("insert into Shop (advertiser, name) values(?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
			statement.setInt(1, advertiserId.intValue());
			statement.setString(2, name);
			statement.executeUpdate();
			Integer id = getGeneratedKey(statement);
			statement.close();
			return id;
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage(), ex);
		}
	}
}
