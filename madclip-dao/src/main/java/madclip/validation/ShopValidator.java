/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.validation;

import java.util.List;

import madclip.model.Advertiser;
import madclip.model.Shop;

public class ShopValidator extends AbstractValidator<Shop> {

	@Override
	protected void validate(Shop entity, List<String> errors) {
		if (entity == null) {
			errors.add("Κενό στοιχείο 'Shop'");
			return;
		}

		String name = entity.getName();

		if (!excludes.contains("name") && name == null || name.trim().equals("")) {
			errors.add("Κενό πεδίο 'Όνομα' σε στοιχείο 'Shop'");
		}

		Advertiser advertiser = entity.getAdvertiser();

		if (!excludes.contains("advertiser") && advertiser == null) {
			errors.add("Κενό πεδίο 'Διαφημιζόμενος' σε στοιχείο 'Shop'");
		}
	}
}
