/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.validation;

import java.util.Date;
import java.util.List;

import madclip.model.Issue;
import madclip.model.Magazine;
import madclip.model.Publisher;

public class IssueValidator extends AbstractValidator<Issue> {

	@Override
	protected void validate(Issue entity, List<String> errors) {
		if (entity == null) {
			errors.add("Κενό στοιχείο 'Τεύχος'");
			return;
		}

		Publisher publisher = entity.getPublisher();

		if (!excludes.contains("publisher") && publisher == null) {
			errors.add("Κενό πεδίο 'Εκδότης' σε στοιχείο 'Τεύχος'");
		}

		Magazine magazine = entity.getMagazine();

		if (!excludes.contains("magazine") && magazine == null) {
			errors.add("Κενό πεδίο 'Περιοδικό' σε στοιχείο 'Τεύχος'");
		}

		Date period = entity.getPeriod();

		if (!excludes.contains("period") && period == null) {
			errors.add("Κενό πεδίο 'Περίοδος' σε στοιχείο 'Τεύχος'");
		}

		String label = entity.getLabel();

		if (!excludes.contains("label") && label == null || label.trim().equals("")) {
			errors.add("Κενό πεδίο 'Ετικέτα' σε στοιχείο 'Τεύχος'");
		}

		int pages = entity.getPages();

		if (!excludes.contains("pages") && pages < 0) {
			errors.add("Λάθος τιμή σε πεδίο 'Σελίδες' σε στοιχείο 'Τεύχος'");
		}
	}
}
