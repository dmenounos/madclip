package madclip.validation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import mojo.dao.DataValidator;

public abstract class AbstractValidator<T> extends DataValidator<T> {

	protected final Set<String> excludes = new HashSet<String>();

	public void setExcludes(String... excludes) {
		Collections.addAll(this.excludes, excludes);
	}
}
