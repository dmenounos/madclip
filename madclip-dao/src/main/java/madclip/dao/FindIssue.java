/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import madclip.model.Issue;

import mojo.dao.DataPage;
import mojo.dao.Specification;
import mojo.dao.jpa.JpaQueryExecutor;

public class FindIssue extends Specification<Issue, DataPage<Issue>> {

	private static final long serialVersionUID = 1L;

	private Integer magazine;
	private Integer extra;
	private String label;
	private Date period;

	public Integer getMagazine() {
		return magazine;
	}

	public void setMagazine(Integer magazine) {
		this.magazine = magazine;
	}

	public Integer getExtra() {
		return extra;
	}

	public void setExtra(Integer extra) {
		this.extra = extra;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Date getPeriod() {
		return period;
	}

	public void setPeriod(Date period) {
		this.period = period;
	}

	public static class Impl extends JpaQueryExecutor<Issue, DataPage<Issue>> {

		@Override
		public Class<?> getType() {
			return FindIssue.class;
		}

		@SuppressWarnings("unchecked")
		public DataPage<Issue> execute(Specification<Issue, DataPage<Issue>> spec) {
			FindIssue findIssue = (FindIssue) spec;

			EntityManager entityManager = getRepository().getEntityManager();
			javax.persistence.Query query = entityManager.createQuery(query(findIssue));
			configQuery(findIssue, query);

			List<Issue> results = (List<Issue>) query.getResultList();
			DataPage<Issue> result = new DataPage<Issue>();
			result.setData(results);
			return result;
		}

		private String query(FindIssue spec) {
			StringBuilder sb = new StringBuilder();
			sb.append("select e from Issue e left join fetch e.publisher left join fetch e.magazine left join fetch e.extra where e.magazine.id = :magazine and e.label = :label and e.period = :period");

			if (spec.getExtra() != null) {
				sb.append(" and e.extra.id = :extra");
			}

			return sb.toString();
		}

		private void configQuery(FindIssue spec, javax.persistence.Query query) {
			query.setParameter("magazine", spec.getMagazine());
			query.setParameter("label", spec.getLabel());
			query.setParameter("period", spec.getPeriod());

			if (spec.getExtra() != null) {
				query.setParameter("extra", spec.getExtra());
			}
		}
	}
}
