/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.dao;

import java.util.Map;

import madclip.model.Trademark;

import mojo.dao.jpa.exec.SelectImpl;
import mojo.dao.spec.Select;

public class SelectTrademarksByCategoryId extends Select<Trademark> {

	private static final long serialVersionUID = 1L;

	private Integer categoryId;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public static class Impl extends SelectImpl<Trademark> {

		@Override
		public Class<?> getType() {
			return SelectTrademarksByCategoryId.class;
		}

		@Override
		protected void from(Select<Trademark> select, StringBuilder sb) {
			SelectTrademarksByCategoryId spec = (SelectTrademarksByCategoryId) select;

			if (spec.getCategoryId() != null) {
				sb.append("TrademarkCategory tc join tc.category c join tc.trademark " + ALIAS);
			}
			else {
				super.from(spec, sb);
			}
		}

		@Override
		protected void criteria(Select<Trademark> select, StringBuilder sb, Map<String, Object> params) {
			SelectTrademarksByCategoryId spec = (SelectTrademarksByCategoryId) select;

			if (spec.getCategoryId() != null) {
				sb.append("c.id = :categoryId");
				params.put("categoryId", spec.getCategoryId());
			}

			super.criteria(select, sb, params);
		}
	}
}
