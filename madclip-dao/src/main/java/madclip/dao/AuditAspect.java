/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.dao;

import java.lang.annotation.Annotation;
import java.util.Date;

import madclip.model.AbstractEntity;
import madclip.model.Audit;
import madclip.model.Auditable;

import mojo.dao.Repository;
import mojo.dao.spec.ById;
import mojo.dao.spec.Delete;
import mojo.dao.spec.Insert;
import mojo.dao.spec.Select;
import mojo.dao.spec.Update;

public class AuditAspect {

	private static final int INSERT = 1;
	private static final int UPDATE = 2;
	private static final int DELETE = 3;

	private AuditContext context;
	private Repository<Audit> repository;

	public AuditContext getContext() {
		return context;
	}

	public void setContext(AuditContext auditContext) {
		this.context = auditContext;
	}

	public Repository<Audit> getRepository() {
		return repository;
	}

	public void setRepository(Repository<Audit> repository) {
		this.repository = repository;
	}

	public void audit(Object query, Object result) {
		int type = 0;

		if (query instanceof Insert<?>) {
			type = INSERT;
		}
		else if (query instanceof Update<?>) {
			type = UPDATE;
		}
		else if (query instanceof Delete<?>) {
			type = DELETE;
		}

		if (type > 0 && isAuditable(result)) {
			AbstractEntity entity = (AbstractEntity) result;

			Audit.Id id = new Audit.Id();
			id.setEntityClass(entity.getClass().getName());
			id.setEntityId(entity.getId());

			Audit audit;

			switch (type) {
			case INSERT:
				audit = new Audit();
				audit.setId(id);
				audit.setCreateDate(new Date());
				audit.setCreateUser(getContext().getUserName());
				getRepository().execute(new Insert<Audit>(audit));
				break;
			case UPDATE:
				audit = getRepository().execute(new Select<Audit>(Audit.class, new ById(id))).unique();

				if (audit != null) {
					audit.setUpdateDate(new Date());
					audit.setUpdateUser(getContext().getUserName());
				}

				break;
			case DELETE:
				audit = getRepository().execute(new Select<Audit>(Audit.class, new ById(id))).unique();

				if (audit != null) {
					getRepository().execute(new Delete<Audit>(Audit.class, id));
				}

				break;
			}
		}
	}

	private boolean isAuditable(Object obj) {
		for (Class<?> klass = obj.getClass(); klass != Object.class; klass = klass.getSuperclass()) {
			for (Annotation annotation : klass.getDeclaredAnnotations()) {
				if (Auditable.class.isAssignableFrom(annotation.annotationType())) {
					return true;
				}
			}
		}

		return false;
	}
}
