/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.dao;

import java.util.Map;

import madclip.model.Advertiser;

import mojo.dao.jpa.exec.SelectImpl;
import mojo.dao.spec.Select;

public class SelectAdvertisersByTrademarkId extends Select<Advertiser> {

	private static final long serialVersionUID = 1L;

	private Integer trademarkId;

	public Integer getTrademarkId() {
		return trademarkId;
	}

	public void setTrademarkId(Integer trademarkId) {
		this.trademarkId = trademarkId;
	}

	public static class Impl extends SelectImpl<Advertiser> {

		@Override
		public Class<?> getType() {
			return SelectAdvertisersByTrademarkId.class;
		}

		@Override
		protected void from(Select<Advertiser> select, StringBuilder sb) {
			SelectAdvertisersByTrademarkId spec = (SelectAdvertisersByTrademarkId) select;

			if (spec.getTrademarkId() != null) {
				sb.append("AdvertiserTrademark at join at.trademark t join at.advertiser " + ALIAS);
			}
			else {
				super.from(spec, sb);
			}
		}

		@Override
		protected void criteria(Select<Advertiser> select, StringBuilder sb, Map<String, Object> params) {
			SelectAdvertisersByTrademarkId spec = (SelectAdvertisersByTrademarkId) select;

			if (spec.getTrademarkId() != null) {
				sb.append("t.id = :trademarkId");
				params.put("trademarkId", spec.getTrademarkId());
			}

			super.criteria(select, sb, params);
		}
	}
}
