/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.model;

import java.io.Serializable;

public class AdSupport implements Serializable {

	private static final long serialVersionUID = 1L;

	private String category;
	private String subCategory;
	private String trademark;
	private String brandname;
	private String advertiser;
	private String shop;
	private String comments;

	/**
	 * @hibernate.property column="supCategory"
	 */
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @hibernate.property column="supSubCategory"
	 */
	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @hibernate.property column="supTrademark"
	 */
	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	/**
	 * @hibernate.property column="supBrandname"
	 */
	public String getBrandname() {
		return brandname;
	}

	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}

	/**
	 * @hibernate.property column="supAdvertiser"
	 */
	public String getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(String advertiser) {
		this.advertiser = advertiser;
	}

	/**
	 * @hibernate.property column="supShop"
	 */
	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	/**
	 * @hibernate.property column="supComments"
	 */
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AdSupport)) {
			return false;
		}

		return toString().equals(obj.toString());
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return "{ trademark: " + trademark + ", brandname:" + brandname + ", advertiser: " + advertiser + ", shop: " + shop + ", comments: " + comments + "}";
	}
}
