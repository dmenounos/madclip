/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @hibernate.class
 */
public class Publisher extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	private String name;
	private Set<Magazine> magazines;

	/**
	 * @hibernate.property
	 */
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @hibernate.set inverse="true"
	 * @hibernate.key column="publisher"
	 * @hibernate.one-to-many class="madclip.model.Magazine"
	 */
	public Set<Magazine> getMagazines() {
		if (magazines == null) {
			magazines = new HashSet<Magazine>();
		}

		return this.magazines;
	}

	protected void setMagazines(Set<Magazine> magazines) {
		this.magazines = magazines;
	}
}
