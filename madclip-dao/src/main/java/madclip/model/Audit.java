/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @hibernate.class
 */
public class Audit implements Serializable {

	private static final long serialVersionUID = 1L;

	private Id id;
	private String createUser;
	private Date createDate;
	private String updateUser;
	private Date updateDate;

	/**
	 * @hibernate.composite-id
	 */
	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	/**
	 * @hibernate.property not-null="true"
	 */
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @hibernate.property type="timestamp" not-null="true"
	 */
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @hibernate.property
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @hibernate.property type="timestamp"
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public static class Id implements Serializable {

		private static final long serialVersionUID = 1L;

		private String entityClass;
		private Integer entityId;

		/**
		 * @hibernate.key-property
		 */
		public String getEntityClass() {
			return entityClass;
		}

		public void setEntityClass(String entityClass) {
			this.entityClass = entityClass;
		}

		/**
		 * @hibernate.key-property
		 */
		public Integer getEntityId() {
			return entityId;
		}

		public void setEntityId(Integer entityId) {
			this.entityId = entityId;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}

			if (!(obj instanceof Id)) {
				return false;
			}

			return toString().equals(obj.toString());
		}

		@Override
		public int hashCode() {
			return toString().hashCode();
		}

		@Override
		public String toString() {
			return "{ entityClass: " + entityClass + ", entityId: " + entityId + " }";
		}
	}
}
