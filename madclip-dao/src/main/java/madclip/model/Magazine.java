/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @hibernate.class
 */
public class Magazine extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	private String title;
	private Publisher publisher;
	private Set<Extra> extras;

	/**
	 * @hibernate.property
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @hibernate.many-to-one not-null="true"
	 */
	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	/**
	 * @hibernate.set inverse="true"
	 * @hibernate.key column="magazine"
	 * @hibernate.one-to-many class="madclip.model.Extra"
	 */
	public Set<Extra> getExtras() {
		if (extras == null) {
			extras = new HashSet<Extra>();
		}

		return extras;
	}

	protected void setExtras(Set<Extra> extras) {
		this.extras = extras;
	}
}
