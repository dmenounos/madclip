/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.model;

/**
 * @hibernate.class
 */
public class Ad extends AuditableEntity {

	private static final long serialVersionUID = 1L;

	private Issue issue;
	private int page;
	private int baseType;
	private AdType exactType;
	private DiscretePosition position;
	private boolean affix;
	private Reportage reportage;
	private boolean totalLook;
	private AdSize size;
	private Category category;
	private SubCategory subCategory;
	private Trademark trademark;
	private Brandname brandname;
	private Advertiser advertiser;
	private Shop shop;
	private String comments;
	private AdSupport support;

	/**
	 * @hibernate.many-to-one not-null="true"
	 */
	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	/**
	 * @hibernate.property
	 */
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @hibernate.property
	 */
	public int getBaseType() {
		return baseType;
	}

	public void setBaseType(int baseType) {
		this.baseType = baseType;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public AdType getExactType() {
		return exactType;
	}

	public void setExactType(AdType exactType) {
		this.exactType = exactType;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public DiscretePosition getPosition() {
		return position;
	}

	public void setPosition(DiscretePosition position) {
		this.position = position;
	}

	/**
	 * @hibernate.property
	 */
	public boolean isAffix() {
		return affix;
	}

	public void setAffix(boolean affix) {
		this.affix = affix;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Reportage getReportage() {
		return reportage;
	}

	public void setReportage(Reportage reportage) {
		this.reportage = reportage;
	}

	/**
	 * @hibernate.property
	 */
	public boolean isTotalLook() {
		return totalLook;
	}

	public void setTotalLook(boolean totalLook) {
		this.totalLook = totalLook;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public AdSize getSize() {
		return size;
	}

	public void setSize(AdSize size) {
		this.size = size;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Trademark getTrademark() {
		return trademark;
	}

	public void setTrademark(Trademark trademark) {
		this.trademark = trademark;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Brandname getBrandname() {
		return brandname;
	}

	public void setBrandname(Brandname brandname) {
		this.brandname = brandname;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Advertiser getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(Advertiser advertiser) {
		this.advertiser = advertiser;
	}

	/**
	 * @hibernate.many-to-one
	 */
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @hibernate.property
	 */
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @hibernate.component
	 */
	public AdSupport getSupport() {
		return support;
	}

	public void setSupport(AdSupport support) {
		this.support = support;
	}
}
