Ext.onReady(function() {
	Ext.QuickTips.init();

	this.topPanel = new Ext.Panel({
		region: 'north',
		contentEl: 'header',
		border: false
	});

	this.sidePanel = new Ext.TabPanel({
		region: 'west',
		split: true,
		collapsible: true,
		collapseMode: 'mini',
		hideCollapseTool: true,
		width: 230,
		minSize: 230,
		maxSize: 300,
		margins: '8 0 8 8',
		border: false,
		activeTab: 0,
		items: new Ext.tree.TreePanel({
			title: 'Menu',
			border: true,
			closable: false,
			autoScroll: true,
			dataUrl: 'navigation.js',
			root: new Ext.tree.AsyncTreeNode({
				id: 0,
				text: 'Menu'
			}),
			rootVisible: false,
			listeners: {
				click: {
					fn: function(node, event) {
						var panel = this.mainPanel.getComponent(node.id);
						
						if (!panel) {
							panel = eval('new ' + node.id + '({ id: "' + node.id + '", closable: true, border: false });');
							this.mainPanel.add(panel);
						}
						
						this.mainPanel.setActiveTab(panel);
					},
					scope: this
				}
			}
		})
	});

	this.mainPanel = new Ext.TabPanel({
		region: 'center',
		margins: '8 8 8 3',
		border: false,
		activeTab: 0,
		items: new Ext.Panel( {
			title: 'Home',
			border: true,
			closable: false,
			autoScroll: true
		})
	});

	this.viewport = new Ext.Viewport({
		layout: 'border',
		items: [ this.topPanel, this.sidePanel, this.mainPanel ]
	});
}, {});
