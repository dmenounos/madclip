Ext.namespace('madclip.ads');

madclip.ads.AdFormPlugin = Ext.extend(mojo.FormPlugin, {

	editHandler: function(grid, rowIndex, ev) {
		var record = grid.getStore().getAt(rowIndex);
		var win = this.createWindow();

		win.on('show', function(window) {
			var query = new FindByIdQuery();
			query.id = record.get('id');
			query.joins = [
				'issue', 
				'issue.publisher', 
				'issue.magazine', 
				'issue.extra',
				'size',
				'category',
				'trademark',
				'brandname',
				'advertiser',
				'shop'
			];

			AdController.find(query, function(entity) {
				var formPanel = window.items.itemAt(0);
				this.setData(formPanel, entity);
				this.changeIssueTitle(formPanel, entity.issue.id);
			}.createDelegate(this));
		}, this);

		win.on('submit', function(window, formPanel) {
			var handler = function(window) {
				window.close();
			}

			var chain = this.chainHandlers([this.issueHandler, this.adHandler, handler], this);
			chain[0].apply(this, arguments);
		}, this);

		win.on('close', function() {
			this.grid.store.reload();
		}, this);

		win.show();
	},

	addHandler: function() {
		var win = this.createWindow();

		win.on('submit', function(window, formPanel) {
			var handler = function(window, formPanel, obj) {
				this.setData(formPanel, { issue: obj.issue });
				formPanel.getForm().reset();
				formPanel.find('name', 'page')[0].focus();
			}

			var chain = this.chainHandlers([this.issueHandler, this.adHandler, handler], this);
			chain[0].apply(this, arguments);
		}, this);

		win.on('close', function() {
			this.grid.store.reload();
		}, this);

		win.show();
	},

	issueHandler: function(window, formPanel, obj, dirty, callback) {
		if (!obj.issue.id) {
			Ext.Msg.confirm('', 'Θα δημιουργηθεί νέο τεύχος. Συνέχεια;', function(id) {
				if (id == 'yes') {
					var query = new CreateQuery();
					query.entity = obj.issue;
					
					IssueController.create(query, function(issue) {
						obj.issue.id = issue.id;
						this.changeIssueTitle(formPanel, issue.id);
						callback(window, formPanel, obj, dirty);
					}.createDelegate(this));
				}
			}, this);
		} else if (dirty['issue']) {
			Ext.Msg.confirm('', 'Θα ενημερωθούν οι αλλαγές του τεύχους. Συνέχεια;', function(id) {
				if (id == 'yes') {
					var query = new UpdateQuery();
					query.entity = obj.issue;
					
					IssueController.update(query, function(issue) {
						callback(window, formPanel, obj, dirty);
					});
				}
			});
		} else {
			callback(window, formPanel, obj, dirty);
		}
	},

	adHandler: function(window, formPanel, obj, dirty, callback) {
		if (!obj.id) {
			var query = new CreateQuery();
			query.entity = obj;
			
			AdController.create(query, function(ad) {
				callback(window, formPanel, ad);
			});
		} else if (dirty['']) {
			var query = new UpdateQuery();
			query.entity = obj;
			
			AdController.update(query, function(ad) {
				callback(window, formPanel, ad);
			});
		} else {
			callback(window, formPanel, obj);
		}
	},
	
	changeIssueTitle: function(formPanel, id) {
		var issuePanel = formPanel.items.itemAt(1);
		issuePanel.setTitle(issuePanel.initialConfig.title + ' #' + id);
	}
});
