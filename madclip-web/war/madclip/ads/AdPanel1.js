Ext.namespace('madclip.ads');

madclip.ads.AdPanel1 = Ext.extend(Ext.Panel, {

	title: 'Διαφημίσεις',

	layout: 'fit',

	initComponent: function() {
		var magazineRenderer = function(value, meta, record) {
			var extra = record.get('extra');
			return value + (extra ? ' / ' + extra : '');
		}

		var periodRenderer = Ext.util.Format.dateRenderer('m/Y');

		var supportRenderer = function(supportField) {
			return function (value, meta, record) {
				if (!value) {
					value = record.get(supportField);
					meta.css = 'mc-eg-rec-support';
				}

				return value;
			}
		}

		var trademarkRenderer = supportRenderer('supportTrademark');
		var advertiserRenderer = supportRenderer('supportAdvertiser');
		var brandnameRenderer = supportRenderer('supportBrandname');

		var adPanel = new mojo.EntityGrid({
			loadMask: true,
			columns: [
				{ dataIndex: 'id', header: '#', width: 40 },
				{ dataIndex: 'magazine', header: 'Περιοδικό', width: 200, renderer: magazineRenderer },
				{ dataIndex: 'period', header: 'Ημ/νια', width: 60, renderer: periodRenderer },
				{ dataIndex: 'label', header: 'Ετικέτα', width: 50 },
				{ dataIndex: 'page', header: 'Σελίδα', width: 50 },
				{ dataIndex: 'category', header: 'Κατηγορία', width: 120, search: 'category.name' },
				{ dataIndex: 'subCategory1', header: 'Είδος', width: 120, search: 'subCategory1.name' },
				{ dataIndex: 'trademark', header: 'Trademark', width: 120, renderer: trademarkRenderer, search: 'trademark.name' },
				{ dataIndex: 'advertiser', header: 'Διαφημιζόμενος', width: 140, renderer: advertiserRenderer, search: 'advertiser.name' },
				{ dataIndex: 'brandname', header: 'Brandname', width: 140, renderer: brandnameRenderer, search: 'brandname.name' }
			],
			store: new mojo.XmlStore({
				record: madclip.ads.Ad,
				controller: 'ad',
				fetchDefaults: {
					order: 'issue.period, issue.magazine.title, issue.extra.title, page',
					joins: [ 
						'issue', 
						'issue.magazine', 
						'issue.extra',
						'category',
						'subCategory1',
						'trademark',
						'brandname',
						'advertiser'
					] 
				},
				view: 'madclip.view.AdView1'
			}),
			plugins: [
				new mojo.FilterPlugin(),
				new madclip.ads.AdFormPlugin({
					winConfig: { title: 'Διαφήμιση', width: 720 },
					formConfig: madclip.ads.AdFormConfig,
					types: { '' : Ad, 'issue' : Issue }
				})
			]
		});

		this.items = [ adPanel ];

		madclip.ads.AdPanel1.superclass.initComponent.call(this);
	}
});
