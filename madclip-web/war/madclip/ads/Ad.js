Ext.namespace('madclip.ads');

madclip.ads.Ad = mojo.record.create([
	{ name: 'id'                , type: 'int' },
	{ name: 'magazine'          , type: 'string' },
	{ name: 'extra'             , type: 'string' },
	{ name: 'period'            , type: 'date' },
	{ name: 'label'             , type: 'string' },
	{ name: 'page'              , type: 'int' },
	{ name: 'baseType'          , type: 'int' },
	{ name: 'size'              , type: 'string' },
	{ name: 'category'          , type: 'string' },
	{ name: 'subCategory1'      , type: 'string' },
	{ name: 'trademark'         , type: 'string' },
	{ name: 'brandname'         , type: 'string' },
	{ name: 'advertiser'        , type: 'string' },
	{ name: 'shop'              , type: 'string' },
	{ name: 'supportTrademark'  , type: 'string' },
	{ name: 'supportBrandname'  , type: 'string' },
	{ name: 'supportAdvertiser' , type: 'string' },
	{ name: 'supportShop'       , type: 'string' }
]);
