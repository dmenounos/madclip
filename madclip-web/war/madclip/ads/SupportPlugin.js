Ext.namespace('oojo');

mojo.SupportPlugin = Ext.extend(Object, {

	constructor: function(config) {
		Ext.apply(this, config);
	},

	init: function(combo) {
		this.combo = combo;
		this.combo.onRender = this.onRender;
		this.combo.setValue = this.setValue;
	},

	onRender: function(cnt, pos) {
		mojo.EntityCombo.superclass.onRender.call(this, cnt, pos);

		this.supportField = new Ext.form.Hidden({
			name: this.support,
			entityCombo: this,
			setValue: function(value) {
				Ext.form.Hidden.prototype.setValue.call(this, value);

				if (value) {
					this.entityCombo.setSupportMode(true);
					this.entityCombo.setValue(value);
				}
			}
		});

		var formPanel = this.findParentByType(Ext.form.FormPanel);
		formPanel.add(this.supportField);

		new Ext.KeyMap(this.el, {
			key: [ Ext.EventObject.BACKSPACE, Ext.EventObject.DELETE ],
			shift: true,
			fn: function() {
				if (this.isExpanded()) {
					this.collapse();
				}

				this.clearValue();
				this.lastQuery = null;
				this.setSupportMode(!this.supportMode);
			},
			scope: this
		});
	},

	setValue: function(value) {
		if (value != undefined && value != null) {
			if (typeof value == 'object') {
				var record = this.store.getById(value.id);

				if (!record) {
					this.store.add(new this.store.record(value, value.id));
				}

				value = Number(value.id);
			}

			if (typeof value == 'number') {
				if (this.supportMode) {
					var record = this.store.getById(value);
					value = record.get(this.displayField);
				}
				else {
					this.supportField.setRawValue('');
				}
			}
		}
		else {
			this.setSupportMode(false);
		}

		if (typeof value == 'string') {
			if (this.supportMode) {
				this.supportField.setRawValue(value);
			}
			else {
				value = '';
				this.supportField.setRawValue('');
			}
		}

		mojo.EntityCombo.superclass.setValue.call(this, value);
	},

	setSupportMode: function(supportMode) {
		this.supportMode = supportMode;

		if (this.supportMode) {
			this.el.addClass('mc-support-field');
		}
		else {
			this.el.removeClass('mc-support-field');
		}
	}
});
