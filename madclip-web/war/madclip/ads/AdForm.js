Ext.namespace('madclip.ads');

madclip.ads.AdFormConfig = function() {
	var idField = new Ext.form.NumberField({
		name: prefix + 'id'
	});

	var issuePanel = Ext.applyIf({
		xtype: 'fieldset',
		title: 'Τεύχος',
		autoHeight: true,
		bodyStyle: 'padding: 0px',
		tools: [
			{
				id: 'refresh',
				qtip: 'Φόρτωση τεύχους',
				handler: function(ev, el, panel) {
					var magazineField = panel.find('name', 'issue.magazine')[0];
					var extraField = panel.find('name', 'issue.extra')[0];
					var periodField = panel.find('name', 'issue.period')[0];
					var labelField = panel.find('name', 'issue.label')[0];

					var magazine = magazineField.getValue();
					var magazineId = magazine ? magazine.id : null;

					var extra = extraField.getValue();
					var extraId = extra ? extra.id : null;

					var period = periodField.getValue() || null;
					var label = labelField.getValue() || null;

					if (!magazineId || !period || !label) {
						Ext.Msg.alert('', 'Πρέπει να έχουν συμπληρωθεί τα πεδία:<br /><br />1. Περιοδικό<br/>2. Extra <span style="font-style: italic;">(προεραιτικό)</span><br/>3. Ημερομηνία<br/>4. Ετικέτα');
					} else {
						var query = new FindIssueQuery();
						query.magazine = magazineId;
						query.extra = extraId;
						query.label = label;
						query.period = period;

						IssueController.find(query, function(issue) {
							if (!issue) {
								Ext.Msg.alert('', 'Δεν βρέθηκε τεύχος.');
							} else {
								var formPanel = panel.findParentByType(Ext.form.FormPanel);
								var data = mojo.FormPlugin.prototype.getData.call(null, formPanel);
								Ext.apply(data, { issue: issue });
								mojo.FormPlugin.prototype.setData.call(null, formPanel, data);
								panel.setTitle(panel.initialConfig.title + ' #' + issue.id);
								formPanel.getForm().reset();
								formPanel.find('name', 'page')[0].focus();
							}
						});
					}
				}
			}
		],
	}, madclip.param.IssueFormConfig('issue'));

	var pageField = Ext.form.NumberField({
		tabIndex: 7,
		fieldLabel: 'Σελίδα',
		name: 'page',
		allowBlank: false,
		allowDecimals: false,
		allowNegative: false,
		validateOnBlur: false
	});

	var baseTypeCombo = Ext.form.ComboBox({
		tabIndex: 8,
		fieldLabel: 'Είδος διαφήμισης',
		name: 'baseType',
		valueField: 'value',
		displayField: 'display',
		typeAhead: true,
		allowBlank: false,
		validateOnBlur: false,
		mode: 'local',
		store: new Ext.data.SimpleStore({
			id: 0,
			fields: ['value', 'display'],
			data: [[1, 'ΑΜΕΣΗ'], [2, 'ΕΜΜΕΣΗ']]
		})
	});

	var sizeField = mojo.EntityCombo({
		tabIndex: 9,
		fieldLabel: 'Μέγεθος', 
		name: 'size',
		displayField: 'name',
		minChars: 2,
		store: new mojo.XmlStore({
			record: madclip.param.AdSize,
			controller: 'ad-size',
			fetchDefaults: { order: 'size' }
		})
	});

	var categoryCombo = new mojo.EntityCombo({
		tabIndex: 10,
		fieldLabel: 'Κατηγορία', 
		name: 'category',
		displayField: 'name',
		store: new mojo.XmlStore({
			record: madclip.param.Category,
			controller: 'category',
			fetchDefaults: { order: 'name' }
		})
	});

	var subCategory1Combo = new mojo.EntitySubCombo({
		tabIndex: 11,
		parentCombo: categoryCombo,
		fieldLabel: 'Είδος', 
		name: 'subCategory1',
		support: 'support.subCategory1',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.SubCategory1,
			controller: 'category-sub-categories-1',
			fetchDefaults: { order: 'name' }
		})
	});

	var subCategory2Combo = new mojo.EntitySubCombo({
		tabIndex: 12,
		parentCombo: categoryCombo,
		fieldLabel: 'Υποείδος', 
		name: 'subCategory2',
		support: 'support.subCategory2',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.SubCategory2,
			controller: 'category-sub-categories-2',
			fetchDefaults: { order: 'name' }
		})
	});

	var trademarkCombo = new mojo.EntitySubCombo({
		tabIndex: 13,
		parentCombo: categoryCombo,
		linkProperty: 'categoryId',
		fieldLabel: 'Trademark', 
		name: 'trademark',
		support: 'support.trademark',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.Trademark,
			controller: 'trademark',
			fetchType: FetchTrademarksByCategoryIdQuery,
			fetchDefaults: { order: 'name' }
		})
	});

	var brandnameCombo = new mojo.EntitySubCombo({
		tabIndex: 14,
		parentCombo: trademarkCombo,
		fieldLabel: 'Brandname',
		name: 'brandname',
		support: 'support.brandname',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.Brandname,
			controller: 'trademark-brandnames',
			fetchDefaults: { order: 'name' }
		})
	});

	var advertiserCombo = new mojo.EntitySubCombo({
		tabIndex: 15,
		parentCombo: trademarkCombo,
		linkProperty: 'trademarkId',
		fieldLabel: 'Διαφημιζόμενος',
		name: 'advertiser',
		support: 'support.advertiser',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.Advertiser,
			controller: 'advertiser',
			fetchType: FetchAdvertisersByTrademarkIdQuery,
			fetchDefaults: { order: 'name' }
		})
	});

	var shopCombo = new mojo.EntitySubCombo({
		tabIndex: 16,
		parentCombo: advertiserCombo,
		fieldLabel: 'Shop',
		name: 'shop',
		support: 'support.shop',
		displayField: 'name',
		pageSize: 100,
		store: new mojo.XmlStore({
			record: madclip.param.Shop,
			controller: 'advertiser-shops',
			fetchDefaults: { order: 'name' }
		})
	});

	return {
		bodyStyle: 'padding: 5px',
		items: [
			{ 
				hidden: true, 
				items: [ idField ] 
			},
			issuePanel,
			{
				border: false,
				bodyStyle: 'padding-top: 5px',
				layout: 'column',
				items: [
					{
						columnWidth: .5,
						border: false,
						bodyStyle: 'padding-left: 10px',
						labelWidth: 110,
						layout: 'form',
						items: [ pageField, sizeField, subCategory1Combo, trademarkCombo, advertiserCombo ]
					},
					{
						columnWidth: .5,
						border: false,
						labelWidth: 120,
						layout: 'form',
						items: [ baseTypeCombo, categoryCombo, subCategory2Combo, brandnameCombo, shopCombo ]
					}
				]
			},
			{
				border: false,
				bodyStyle: 'padding-left: 10px; padding-bottom: 10px;',
				labelWidth: 110,
				layout: 'form',
				items: [
					{ tabIndex: 17, xtype: 'textfield', fieldLabel: 'Παρατηρήσεις', name: 'comments', width: 549 },
					{ tabIndex: 18, xtype: 'textfield', fieldLabel: 'Σχόλια', name: 'support.comments', width: 549 }
				]
			}
		]
	};
}
