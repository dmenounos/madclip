Ext.namespace('madclip.param');

madclip.param.AdTypePanel = Ext.extend(Ext.Panel, {

	title: 'Τύποι Διαφήμισης',

	layout: 'fit',

	initComponent: function() {
		var baseTypeRenderer = function(value) {
			return value ? value == 1 ? 'ΑΜΕΣΗ' : 'ΕΜΜΕΣΗ' : '';
		}

		var baseTypeEditor = new Ext.form.ComboBox({
			valueField: 'value',
			displayField: 'display',
			typeAhead: true,
			mode: 'local',
			store: new Ext.data.SimpleStore({
				id: 0,
				fields: ['value', 'display'],
				data: [[1, 'ΑΜΕΣΗ'], [2, 'ΕΜΜΕΣΗ']]
			})
		});

		var typePanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'baseType', header: 'Βασικό', width: 100, sortable: true, editor: baseTypeEditor, renderer: baseTypeRenderer },
				{ dataIndex: 'description', header: 'Περιγραφή', width: 200, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.AdType,
				controller: 'ad-type'
			})
		});

		this.items = [ typePanel ];

		madclip.param.AdTypePanel.superclass.initComponent.call(this);
	}
});
