Ext.namespace('madclip.param');

madclip.param.PublisherPanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var publisherPanel = new mojo.EntityGrid({
			name: 'Εκδοτικοί οργανισμοί',
			columns: [
				{ dataIndex: 'name', header: 'Επωνυμία', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Publisher,
				controller: 'publisher'
			})
		});

		var magazinesPanel = new mojo.EntitySubGrid({
			parentGrid: publisherPanel,
			name: 'Περιοδικά',
			readOnly: true,
			columns: [
				{ dataIndex: 'title', header: 'Τίτλος', width: 300, sortable: true, search: true }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Magazine,
				controller: 'publisher-magazines'
			})
		});

		this.master = publisherPanel;
		this.details = [ magazinesPanel ];

		madclip.param.PublisherPanel.superclass.initComponent.call(this);
	}
});
