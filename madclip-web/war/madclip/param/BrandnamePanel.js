Ext.namespace('madclip.param');

madclip.param.BrandnamePanel = Ext.extend(Ext.Panel, {

	title: 'Brandnames',

	layout: 'fit',

	initComponent: function() {
		var trademarkCombo = new mojo.EntityCombo({
			displayField: 'name',
			pageSize: 100,
			store: new mojo.XmlStore({
				record: madclip.param.Trademark,
				controller: 'trademark',
				fetchDefaults: { order: 'name' }
			})
		});

		var brandnanePanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'trademark', header: 'Trademark', width: 300, sortable: true, search: true, editor: trademarkCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Brandname,
				controller: 'brandname',
				fetchDefaults: { joins: [ 'trademark' ] }
			})
		});

		this.items = [ brandnanePanel ];

		madclip.param.BrandnamePanel.superclass.initComponent.call(this);
	}
});
