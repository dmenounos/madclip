Ext.namespace('madclip.param');

madclip.param.Publisher = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'name' , type: 'string' }
], 'Publisher');
