Ext.namespace('madclip.param');

madclip.param.IssueFormConfig = function(prefix) {
	prefix = prefix ? prefix + '.' : '';

	var idField = new Ext.form.NumberField({
		name: prefix + 'id'
	});

	var magCombo = new mojo.EntityCombo({
		tabIndex: 1,
		fieldLabel: 'Περιοδικό',
		name: prefix + 'magazine',
		displayField: 'title',
		allowBlank: false,
		validateOnBlur: false,
		store: new mojo.XmlStore({
			record: madclip.param.Magazine,
			controller: 'magazine',
			fetchDefaults: { joins: [ 'publisher' ], order: 'title' }
		}),
		listeners: {
			change: function(fld, nv, ov) {
				if (nv) {
					pubCombo.setValue(nv.publisher);
				} else {
					pubCombo.clearValue();
				}
			}
		}
	});

	var pubCombo = new mojo.EntityCombo({
		tabIndex: 2,
		fieldLabel: 'Εκδότης',
		name: prefix + 'publisher',
		displayField: 'name',
		allowBlank: false,
		validateOnBlur: false,
		store: new mojo.XmlStore({
			record: madclip.param.Publisher,
			controller: 'publisher',
			fetchDefaults: { order: 'name' }
		})
	});

	var extCombo = new mojo.EntitySubCombo({
		tabIndex: 3,
		parentCombo: magCombo,
		fieldLabel: 'Extra',
		name: prefix + 'extra',
		displayField: 'title',
		store: new mojo.XmlStore({
			record: madclip.param.Extra,
			controller: 'magazine-extras',
			fetchDefaults: { order: 'title' }
		})
	});

	var periodField = new Ext.form.DateField({
		tabIndex: 4,
		fieldLabel: 'Ημερομηνία',
		name: prefix + 'period',
		format: 'd/m/Y',
		altFormats: 'j/n/Y',
		allowBlank: false,
		validateOnBlur: false,
		showToday: false,
		disabledDates: [ '((0[02-9])|([1-3]\\d))/\\d{1,2}/\\d{4}' ]
	});

	var labelField = new Ext.form.TextField({
		tabIndex: 5,
		fieldLabel: 'Ετικέτα',
		name: prefix + 'label',
		allowBlank: false,
		validateOnBlur: false
	});

	var pagesField = new Ext.form.NumberField({
		tabIndex: 6,
		fieldLabel: 'Σελίδες',
		name: prefix + 'pages',
		allowBlank: false,
		allowDecimals: false,
		allowNegative: false,
		validateOnBlur: false
	});

	return {
		bodyStyle: 'padding: 5px',
		items: [
			{ 
				hidden: true, 
				items: [ idField ] 
			},
			{
				border: false,
				layout: 'column',
				items: [
					{
						columnWidth: .5,
						border: false,
						labelWidth: 110,
						layout: 'form',
						items: [ magCombo, extCombo, labelField ]
					},
					{
						columnWidth: .5,
						border: false,
						labelWidth: 120,
						layout: 'form',
						items: [ pubCombo, periodField, pagesField ]
					}
				]
			}
		]
	};
}
