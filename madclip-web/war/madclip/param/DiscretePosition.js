Ext.namespace('madclip.param');

madclip.param.DiscretePosition = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'name' , type: 'string' }
], 'DiscretePosition');
