Ext.namespace('madclip.param');

/**
 * Κατηγορίες - Υπο-Κατηγορίες Είδη
 */
madclip.param.Category = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'name' , type: 'string' },
	{ name: 'category' , convert: 'Category' }
], 'Category');

/**
 * Υπο-Κατηγορίες Υπο-Είδη
 */
madclip.param.SubCategory = mojo.record.create([
	{ name: 'id'       , type: 'int' },
	{ name: 'name'     , type: 'string' },
	{ name: 'category' , convert: 'Category' }
], 'SubCategory');
