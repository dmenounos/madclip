Ext.namespace('madclip.param');

madclip.param.AdSizePanel = Ext.extend(Ext.Panel, {

	title: 'Μεγέθη',

	layout: 'fit',

	initComponent: function() {
		var sizeRenderer = function(value) {
			return value ? value + ' / 12': '';
		}

		var sizeEditor = new Ext.form.NumberField({
			allowDecimals: false,
			allowNegative: false
		});

		var sizePanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'size', header: 'Μέγεθος', width: 100, sortable: true, editor: sizeEditor, renderer: sizeRenderer },
				{ dataIndex: 'name', header: 'Όνομα', width: 200, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.AdSize,
				controller: 'ad-size'
			})
		});

		this.items = [ sizePanel ];

		madclip.param.AdSizePanel.superclass.initComponent.call(this);
	}
});
