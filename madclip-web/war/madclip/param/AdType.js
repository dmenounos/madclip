Ext.namespace('madclip.param');

madclip.param.AdType = mojo.record.create([
	{ name: 'id'          , type: 'int' },
	{ name: 'baseType'    , type: 'int' },
	{ name: 'description' , type: 'string' }
], 'AdType');
