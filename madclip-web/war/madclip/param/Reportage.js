Ext.namespace('madclip.param');

madclip.param.Reportage = mojo.record.create([
	{ name: 'id'        , type: 'int' },
	{ name: 'name'      , type: 'string' },
	{ name: 'magazine'  , convert: 'Magazine' },
	{ name: 'extra'     , convert: 'Extra' }
], 'Reportage');
