Ext.namespace('madclip.param');

madclip.param.Shop = mojo.record.create([
	{ name: 'id'         , type: 'int' },
	{ name: 'name'       , type: 'string' },
	{ name: 'advertiser' , convert: 'Advertiser' }
], 'Shop');
