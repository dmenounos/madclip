Ext.namespace('madclip.param');

madclip.param.MagazinePanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var publisherCombo = new mojo.EntityCombo({
			displayField: 'name',
			store: new mojo.XmlStore({
				record: madclip.param.Publisher,
				controller: 'publisher',
				fetchDefaults: { order: 'name' }
			})
		});

		var magazinePanel = new mojo.EntityGrid({
			name: 'Περιοδικά',
			columns: [
				{ dataIndex: 'title', header: 'Τίτλος', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'publisher', header: 'Εκδότης', width: 300, sortable: true, search: true, editor: publisherCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Magazine,
				controller: 'magazine',
				fetchDefaults: { joins: [ 'publisher' ] }
			})
		});

		var extrasPanel = new mojo.EntitySubGrid({
			parentGrid: magazinePanel,
			name: 'Extras',
			readOnly: true,
			columns: [
				{ dataIndex: 'title', header: 'Τίτλος', width: 300, sortable: true, search: true }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Extra,
				controller: 'magazine-extras'
			})
		});

		this.master = magazinePanel;
		this.details = [ extrasPanel ];

		madclip.param.MagazinePanel.superclass.initComponent.call(this);
	}
});
