Ext.namespace('madclip.param');

/**
 * Κατηγορίες (master)
 * Υπο-Κατηγορίες Είδη (detail)
 * Υπο-Κατηγορίες Υπο-Είδη (detail)
 */
madclip.param.CategoryPanel = Ext.extend(mojo.EntityPanel, {

	ById: mojo.xml.create('ById'),

	initComponent: function() {
		var categoryPanel = new mojo.EntityGrid({
			name: 'Κατηγορίες',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Category,
				controller: 'category',
				fetchDefaults: {
					filters: [ new this.ById({ property: 'category', id: null }) ]
				}
			})
		});

		var categoriesPanel = new mojo.EntitySubGrid({
			parentGrid: categoryPanel,
			name: 'Είδη',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Category,
				controller: 'category'
			})
		});

		var subCategoriesPanel = new mojo.EntitySubGrid({
			parentGrid: categoryPanel,
			name: 'Υποείδη',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.SubCategory,
				controller: 'category-sub-categories'
			})
		});

		this.master = categoryPanel;
		this.details = [ categoriesPanel, subCategoriesPanel ];

		madclip.param.CategoryPanel.superclass.initComponent.call(this);
	}
});

/**
 * Υπο-Κατηγορίες Είδη
 */
madclip.param.CategoriesPanel = Ext.extend(mojo.EntityPanel, {

	ById: mojo.xml.create('ById'),

	initComponent: function() {
		var categoryRenderer = mojo.EntityGrid.prototype.objRenderer('name');

		var categoriesPanel = new mojo.EntityGrid({
			name: 'Είδη',
			readOnly: true,
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'category', header: 'Κατηγορία', width: 300, sortable: true, search: true, renderer: categoryRenderer }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Category,
				controller: 'category',
				fetchDefaults: {
					joins: [ 'category' ],
					filters: [ new this.ById({ property: 'category', id: null, not: true }) ]
				}
			})
		});

		this.master = categoriesPanel;

		madclip.param.CategoriesPanel.superclass.initComponent.call(this);
	}
});

/**
 * Υπο-Κατηγορίες Υπο-Είδη
 */
madclip.param.SubCategoriesPanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var categoryRenderer = mojo.EntityGrid.prototype.objRenderer('name');

		var subCategoriesPanel = new mojo.EntityGrid({
			name: 'Υποείδη',
			readOnly: true,
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'category', header: 'Κατηγορία', width: 300, sortable: true, search: true, renderer: categoryRenderer }
			],
			store: new mojo.XmlStore({
				record: madclip.param.SubCategory,
				controller: 'category-sub-categories',
				fetchDefaults: {
					joins: [ 'category' ]
				}
			})
		});

		this.master = subCategoriesPanel;

		madclip.param.SubCategoriesPanel.superclass.initComponent.call(this);
	}
});
