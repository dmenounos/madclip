Ext.namespace('madclip.param');

madclip.param.ExtraPanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var magazineCombo = new mojo.EntityCombo({
			displayField: 'title',
			store: new mojo.XmlStore({
				record: madclip.param.Magazine,
				controller: 'magazine',
				fetchDefaults: { order: 'title' }
			})
		});

		var extraPanel = new mojo.EntityGrid({
			name: 'Extra περιοδικά',
			columns: [
				{ dataIndex: 'title', header: 'Τίτλος', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'magazine', header: 'Περιοδικό', width: 300, sortable: true, search: true, editor: magazineCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Extra,
				controller: 'extra',
				fetchDefaults: { joins: [ 'magazine' ] }
			})
		});

		this.master = extraPanel;

		madclip.param.ExtraPanel.superclass.initComponent.call(this);
	}
});
