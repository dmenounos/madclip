Ext.namespace('madclip.param');

madclip.param.IssuePanel = Ext.extend(Ext.Panel, {

	title: 'Τεύχη',

	layout: 'fit',

	initComponent: function() {
		var publisherRenderer = mojo.EntityGrid.prototype.objRenderer('name');
		var magazineRenderer = mojo.EntityGrid.prototype.objRenderer('title');
		var extraRenderer = mojo.EntityGrid.prototype.objRenderer('title');
		var periodRenderer = Ext.util.Format.dateRenderer('m/Y');

		var issuePanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'publisher', header: 'Εκδότης', width: 170, sortable: true, search: true, renderer: publisherRenderer },
				{ dataIndex: 'magazine', header: 'Περιοδικό', width: 170, sortable: true, search: true, renderer: magazineRenderer },
				{ dataIndex: 'extra', header: 'Extra', width: 170, sortable: true, search: true, renderer: extraRenderer },
				{ dataIndex: 'period', header: 'Περιόδος', width: 80, sortable: true, renderer: periodRenderer },
				{ dataIndex: 'label', header: 'Ετικέτα', width: 80 },
				{ dataIndex: 'pages', header: 'Σελίδες', width: 80 }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Issue,
				controller: 'issue',
				fetchDefaults: { joins: [ 'publisher', 'magazine', 'extra' ] }
			}),
			plugins: [
				new mojo.FormPlugin({
					winConfig: { title: 'Τεύχος', width: 700 },
					formConfig: madclip.param.IssueFormConfig
				})
			]
		});

		this.items = [ issuePanel ];

		madclip.param.IssuePanel.superclass.initComponent.call(this);
	}
});
