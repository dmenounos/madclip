Ext.namespace('madclip.param');

madclip.param.TrademarkPanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var trademarkPanel = new mojo.EntityGrid({
			name: 'Trademarks',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Trademark,
				controller: 'trademark'
			})
		});

		var categoryCombo = new mojo.EntityCombo({
			displayField: 'name',
			store: new mojo.XmlStore({
				record: madclip.param.Category,
				controller: 'category',
				fetchDefaults: { order: 'name' }
			})
		});

		var categoriesPanel = new mojo.EntitySubGrid({
			parentGrid: trademarkPanel,
			name: 'Κατηγορίες',
			columns: [
				{ dataIndex: 'category', header: 'Κατηγορία', width: 300, sortable: true, search: true, editor: categoryCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.TrademarkCategory,
				controller: 'trademark-categories',
				fetchDefaults: { joins: [ 'category' ] }
			})
		});

		this.master = trademarkPanel;
		this.details = [ categoriesPanel ];

		madclip.param.TrademarkPanel.superclass.initComponent.call(this);
	}
});
