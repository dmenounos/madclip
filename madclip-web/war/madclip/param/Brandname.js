Ext.namespace('madclip.param');

madclip.param.Brandname = mojo.record.create([
	{ name: 'id'        , type: 'int' },
	{ name: 'name'      , type: 'string' },
	{ name: 'trademark' , convert: 'Trademark' }
], 'Brandname');
