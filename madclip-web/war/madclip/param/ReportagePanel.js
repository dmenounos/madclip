Ext.namespace('madclip.param');

madclip.param.ReportagePanel = Ext.extend(Ext.Panel, {

	title: 'Reportage',

	layout: 'fit',

	initComponent: function() {
		var magazineCombo = new mojo.EntityCombo({
			displayField: 'title',
			store: new mojo.XmlStore({
				record: madclip.param.Magazine,
				controller: 'magazine',
				fetchDefaults: { order: 'title' }
			})
		});

//		var extraCombo = new mojo.EntityCombo({
//			displayField: 'title',
//			store: new mojo.XmlStore({
//				record: madclip.param.Extra,
//				controller: 'extra',
//				fetchDefaults: { order: 'title' }
//			})
//		});

		var reportagePanel = new mojo.EntityGrid({
			region: 'center',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'magazine', header: 'Περιοδικό', width: 300, sortable: true, search: true, editor: magazineCombo }
//				{ dataIndex: 'extra', header: 'Extra', width: 300, sortable: true, search: true, editor: extraCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Reportage,
				controller: 'reportage',
				fetchDefaults: { joins: [ 'magazine' /* , 'extra' */ ] }
			})
		});

		this.items = [ reportagePanel ];

		madclip.param.ReportagePanel.superclass.initComponent.call(this);
	}
});
