Ext.namespace('madclip.param');

madclip.param.Magazine = mojo.record.create([
	{ name: 'id'        , type: 'int' },
	{ name: 'title'     , type: 'string' },
	{ name: 'publisher' , convert: 'Publisher' }
], 'Magazine');
