Ext.namespace('madclip.param');

madclip.param.Issue = mojo.record.create([
	{ name: 'id'        , type: 'int' },
	{ name: 'publisher' , convert: 'Publisher' },
	{ name: 'magazine'  , convert: 'Magazine' },
	{ name: 'extra'     , convert: 'Extra' },
	{ name: 'period'    , type: 'date' },
	{ name: 'label'     , type: 'string' },
	{ name: 'pages'     , type: 'int' }
], 'Issue');
