Ext.namespace('madclip.param');

madclip.param.Advertiser = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'name' , type: 'string' }
], 'Advertiser');

madclip.param.AdvertiserTrademark = mojo.record.create([
	{ name: 'id'         , type: 'int' },
	{ name: 'advertiser' , convert: 'Advertiser' },
	{ name: 'trademark'  , convert: 'Trademark' }
], 'AdvertiserTrademark');
