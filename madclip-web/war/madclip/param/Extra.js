Ext.namespace('madclip.param');

madclip.param.Extra = mojo.record.create([
	{ name: 'id'       , type: 'int' },
	{ name: 'title'    , type: 'string' },
	{ name: 'magazine' , convert: 'Magazine' }
], 'Extra');
