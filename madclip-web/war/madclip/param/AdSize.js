Ext.namespace('madclip.param');

madclip.param.AdSize = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'size' , type: 'int' },
	{ name: 'name' , type: 'string' }
], 'AdSize');
