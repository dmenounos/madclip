Ext.namespace('madclip.param');

madclip.param.DiscretePositionPanel = Ext.extend(Ext.Panel, {

	title: 'Διακεκριμένες Θέσεις',

	layout: 'fit',

	initComponent: function() {
		var positionPanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.DiscretePosition,
				controller: 'discrete-position'
			})
		});

		this.items = [ positionPanel ];

		madclip.param.DiscretePositionPanel.superclass.initComponent.call(this);
	}
});
