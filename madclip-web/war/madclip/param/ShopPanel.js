Ext.namespace('madclip.param');

madclip.param.ShopPanel = Ext.extend(Ext.Panel, {

	title: 'Shops',

	layout: 'fit',

	initComponent: function() {
		var advertiserCombo = new mojo.EntityCombo({
			displayField: 'name',
			pageSize: 100,
			store: new mojo.XmlStore({
				record: madclip.param.Advertiser,
				controller: 'advertiser',
				fetchDefaults: { order: 'name' }
			})
		});

		var shopPanel = new mojo.EntityGrid({
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() },
				{ dataIndex: 'advertiser', header: 'Διαφημιζόμενος', width: 300, sortable: true, search: true, editor: advertiserCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Shop,
				controller: 'shop',
				fetchDefaults: { joins: [ 'advertiser' ] }
			})
		});

		this.items = [ shopPanel ];

		madclip.param.ShopPanel.superclass.initComponent.call(this);
	}
});
