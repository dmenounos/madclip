Ext.namespace('madclip.param');

madclip.param.Trademark = mojo.record.create([
	{ name: 'id'   , type: 'int' },
	{ name: 'name' , type: 'string' }
], 'Trademark');

madclip.param.TrademarkCategory = mojo.record.create([
	{ name: 'id'        , type: 'int' },
	{ name: 'trademark' , convert: 'Trademark' },
	{ name: 'category'  , convert: 'Category' }
], 'TrademarkCategory');
