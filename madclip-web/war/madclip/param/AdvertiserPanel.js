Ext.namespace('madclip.param');

madclip.param.AdvertiserPanel = Ext.extend(mojo.EntityPanel, {

	initComponent: function() {
		var advertiserPanel = new mojo.EntityGrid({
			name: 'Διαφημιζόμενοι',
			columns: [
				{ dataIndex: 'name', header: 'Όνομα', width: 300, sortable: true, search: true, editor: new Ext.form.TextField() }
			],
			store: new mojo.XmlStore({
				record: madclip.param.Advertiser,
				controller: 'advertiser'
			})
		});

		var trademarkCombo = new mojo.EntityCombo({
			displayField: 'name',
			pageSize: 100,
			store: new mojo.XmlStore({
				record: madclip.param.Trademark,
				controller: 'trademark',
				fetchDefaults: { order: 'name' }
			})
		});

		var trademarksPanel = new mojo.EntitySubGrid({
			parentGrid: advertiserPanel,
			name: 'Trademarks',
			columns: [
				{ dataIndex: 'trademark', header: 'Trademark', width: 300, sortable: true, search: true, editor: trademarkCombo }
			],
			store: new mojo.XmlStore({
				record: madclip.param.AdvertiserTrademark,
				controller: 'advertiser-trademarks',
				fetchDefaults: { joins: [ 'trademark' ] }
			})
		});

		this.master = advertiserPanel;
		this.details = [ trademarksPanel ];

		madclip.param.AdvertiserPanel.superclass.initComponent.call(this);
	}
});
