[
	{
		id: 'madclip.ads',
		text: 'Διαφημίσεις',
		expanded: true,
		children: [
			{
				id: 'madclip.ads.AdPanel1',
				text: 'Ανα: Ημερομηνία, Περιοδικό',
				leaf: true
			}
		]
	},
	{
		id: 'madclip.data',
		text: 'Δεδομένα',
		expanded: true,
		children: [
			{
				id: 'madclip.param.IssuePanel',
				text: 'Τεύχη',
				leaf: true
			},
			{
				id: 'madclip.param.PublisherPanel',
				text: 'Εκδοτικοί Οργανισμοί',
				leaf: true
			},
			{
				id: 'madclip.param.MagazinePanel',
				text: 'Περιοδικά',
				leaf: true
			},
			{
				id: 'madclip.param.ExtraPanel',
				text: 'Extra Περιοδικά',
				leaf: true
			},
			{
				id: 'madclip.param.ReportagePanel',
				text: 'Reportage',
				leaf: true
			},
			{
				id: 'madclip.param.AdSizePanel',
				text: 'Μεγέθη',
				leaf: true
			},
			{
				id: 'madclip.param.AdTypePanel',
				text: 'Τύποι Διαφήμισης',
				leaf: true
			},
			{
				id: 'madclip.param.DiscretePositionPanel',
				text: 'Διακεκριμένες Θέσεις',
				leaf: true
			},
			{
				id: 'madclip.param.CategoryPanel',
				text: 'Κατηγορίες',
				leaf: true
			},
			{
				id: 'madclip.param.CategoriesPanel',
				text: 'Είδη',
				leaf: true
			},
			{
				id: 'madclip.param.SubCategoriesPanel',
				text: 'Υποείδη',
				leaf: true
			},
			{
				id: 'madclip.param.TrademarkPanel',
				text: 'Trademarks',
				leaf: true
			},
			{
				id: 'madclip.param.BrandnamePanel',
				text: 'Brandnames',
				leaf: true
			},
			{
				id: 'madclip.param.AdvertiserPanel',
				text: 'Διαφημιζόμενοι',
				leaf: true
			},
			{
				id: 'madclip.param.ShopPanel',
				text: 'Shops',
				leaf: true
			}
		]
	},
	{
		id: 'madclip.tools',
		text: 'Εργαλεία',
		children: []
	}
]
