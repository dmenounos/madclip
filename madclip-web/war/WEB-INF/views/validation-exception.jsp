<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<fmt:setBundle basename="site" />
	<head>
		<title><fmt:message key="error.title" /></title>
	</head>
	<body>
		<p class="exception">
			<c:forEach items="${exception.errors}" var="error">
				<fmt:message key="${error}" />
			</c:forEach>
		</p>
	</body>
</html>
