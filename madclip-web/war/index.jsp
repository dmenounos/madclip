<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="jwr" uri="http://jawr.net/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<fmt:setBundle basename="site" scope="session" />
	<head>
		<meta name="Copyright" content="Copyright 2009 Dimitrios Menounos" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<title><fmt:message key="title" /></title>

		<link type="text/css" rel="stylesheet" href="ext-3.3.0/resources/css/ext-all.css" />
		<script type="text/javascript" src="ext-3.3.0/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="ext-3.3.0/ext-all-debug.js"></script>

		<link type="text/css" rel="stylesheet" href="styles.css" />

		<jwr:script src="/bundles/mojo.js" />
		<jwr:script src="/bundles/madclip.js" />

		<fmt:message key="site.js" var="i18n" />
		<jwr:script src="${i18n}" />
	</head>
	<body>
		<div id="page">
			<div id="header">
				<h2><fmt:message key="title" /></h2>
				<p class="iconDate"> <jsp:useBean id="date" class="java.util.Date" /> <fmt:formatDate value="${date}" dateStyle="full" /> </p>
				<p class="iconUser"> ${pageContext.request.remoteUser} </p>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>
