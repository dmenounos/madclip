<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mojo" uri="http://mojo.gr/jsp/taglib" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Test</title>
		<style type="text/css">
			table {
				border: 0;
			}
			
			tbody td {
				padding: 2px 8px;
			}
			
			tbody .label {
				text-align: right;
			}
		</style>
	</head>
	<body>
		<h2>Request Headers (<a href="?allheaders=1">all</a>)</h2>
		<table cellspacing="0">
			<tbody>
				<c:choose>
					<c:when test="${empty param.allheaders}">
						<c:forTokens items="accept, accept-charset, accept-language" delims=", " var="current">
							<tr>
								<td class="label">${current}:</td>
								<td>${header[current]}</td>
							</tr>
						</c:forTokens>
					</c:when>
					<c:otherwise>
						<%
							java.util.TreeMap<String, String> sh = new java.util.TreeMap<String, String>();
							
							for (java.util.Enumeration<?> e = request.getHeaderNames(); e.hasMoreElements();) {
								String h = (String) e.nextElement();
								sh.put(h, request.getHeader(h));
							}
							
							pageContext.setAttribute("sh", sh);
						%>
						<c:forEach items="${sh}" var="h">
							<tr>
								<td class="label">${h.key}:</td>
								<td>${h.value}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<h2>Request Properties</h2>
		<ul>
			<li>serverName: ${pageContext.request.serverName}</li>
			<li>serverPort: ${pageContext.request.serverPort}</li>
		</ul>
		<ul>
			<li>requestURL: ${pageContext.request.requestURL}</li>
			<li>queryString: ${pageContext.request.queryString}</li>
		</ul>
		<ul>
			<li>requestURI: ${pageContext.request.requestURI}</li>
			<li>contextPath: ${pageContext.request.contextPath}</li>
			<li>servletPath: ${pageContext.request.servletPath}</li>
		</ul>
		<ul>
			<li>pathInfo: ${pageContext.request.pathInfo}</li>
			<li>pathTranslated: ${pageContext.request.pathTranslated}</li>
		</ul>
		<ul>
			<li>locale: ${pageContext.request.locale}
				<ul>
					<li>locale.language: ${pageContext.request.locale.language}</li>
					<li>locale.country: ${pageContext.request.locale.country}</li>
				</ul>
			</li>
		</ul>
		
		<form method="post">
			<input type="text" name="test" /> <input type="submit" value="Submit" />
			<c:if test="${not empty param.test}">${param.test}</c:if>
		</form>
		
		<%
			pageContext.setAttribute("str", new String());
			pageContext.setAttribute("num", new Integer(0));
		%>
		<ul>
			<li>instanceOf(String, Number): ${mojo:instanceOf(str, 'java.lang.Number')}</li>
			<li>instanceOf(Integer, Number): ${mojo:instanceOf(num, 'java.lang.Number')}</li>
		</ul>
	</body>
</html>
