Ext.apply(mojo.EntityGrid.prototype, {

	msgAdd: 'New',
	msgRemove: 'Delete',
	msgSubmit: 'Submit'
});

Ext.apply(mojo.FilterPlugin.prototype, {

	msgSearch: 'Search',
	msgApply: 'Apply'
});

Ext.apply(mojo.FormPlugin.prototype, {

	msgOk: 'Ok',
	msgClose: 'Close',
	msgDiscardChanges: 'Changes will be lost. Continue?'
});
