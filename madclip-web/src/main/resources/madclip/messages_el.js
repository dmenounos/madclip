Ext.apply(mojo.EntityGrid.prototype, {

	msgAdd: 'Νέο',
	msgRemove: 'Διαγραφή',
	msgSubmit: 'Εφαρμογή'
});

Ext.apply(mojo.FilterPlugin.prototype, {

	msgSearch: 'Αναζήτηση',
	msgApply: 'Εφαρμογή'
});

Ext.apply(mojo.FormPlugin.prototype, {

	msgOk: 'Αποδοχή',
	msgClose: 'Κλείσιμο',
	msgDiscardChanges: 'Οι αλλαγές θα χαθούν. Συνέχεια;'
});
