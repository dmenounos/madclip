package madclip.web;

import javax.servlet.http.HttpServletRequest;

import madclip.dao.AuditContext;

import mojo.web.WebContextFilter;

public class WebContext implements AuditContext {

	@Override
	public String getUserName() {
		HttpServletRequest request = (HttpServletRequest) WebContextFilter.getRequest();
		return request.getRemoteUser();
	}

	@Override
	public String getUserHost() {
		HttpServletRequest request = (HttpServletRequest) WebContextFilter.getRequest();
		return request.getRemoteHost();
	}

	@Override
	public boolean isUserInRole(String role) {
		HttpServletRequest request = (HttpServletRequest) WebContextFilter.getRequest();
		return request.isUserInRole(role);
	}
}
