package madclip.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import madclip.model.AbstractEntity;

import mojo.dao.DataPage;
import mojo.dao.DataService;
import mojo.dao.spec.Batch;
import mojo.dao.spec.Select;

public class DataController<E extends AbstractEntity> {

	private DataService<E> service;

	public DataService<E> getService() {
		return service;
	}

	public void setService(DataService<E> service) {
		this.service = service;
	}

	@ResponseBody
	@RequestMapping("/persist.do")
	public int persist(@RequestBody Batch<E> batch) {
		service.persist(batch);
		return 0;
	}

	@ResponseBody
	@RequestMapping("/find.do")
	public E find(@RequestBody Select<E> spec) {
		return service.select(spec).unique();
	}

	@ResponseBody
	@RequestMapping("/fetch.do")
	public DataPage<E> fetch(@RequestBody Select<E> spec) {
		return service.select(spec);
	}

	@ResponseBody
	@RequestMapping("/fetch-view.do")
	public DataPage<?> fetchView(@RequestBody Select<E> spec, @RequestParam String view) {
		try {
			DataPage<E> page = service.select(spec);
			List<Object> views = new ArrayList<Object>(page.getData().size());
			Class<?> viewClass = Class.forName(view);

			for (E entity : page.getData()) {
				Object obj = viewClass.getConstructor(entity.getClass()).newInstance(entity);
				views.add(obj);
			}

			DataPage<Object> result = new DataPage<Object>();
			result.setTotal(page.getTotal());
			result.setData(views);
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
