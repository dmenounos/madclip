/*
 * Copyright (C) 2008 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package madclip.view;

import java.util.Date;

import madclip.model.Ad;
import madclip.model.AdSupport;
import madclip.model.Advertiser;
import madclip.model.Brandname;
import madclip.model.Category;
import madclip.model.Extra;
import madclip.model.Issue;
import madclip.model.Magazine;
import madclip.model.Trademark;

public class AdView1 {

	private int id;
	private String magazine;
	private String extra;
	private Date period;
	private String label;
	private int page;
	private String category;
	private String trademark;
	private String brandname;
	private String advertiser;
	private String supportTrademark;
	private String supportBrandname;
	private String supportAdvertiser;

	public AdView1(Ad ad) {
		Issue issue = ad.getIssue();
		Magazine magazine = issue.getMagazine();
		Extra extra = issue.getExtra();
		Category category = ad.getCategory();
		Trademark trademark = ad.getTrademark();
		Brandname brandname = ad.getBrandname();
		Advertiser advertiser = ad.getAdvertiser();
		AdSupport support = ad.getSupport();

		setId(ad.getId().intValue());
		setMagazine(magazine.getTitle());

		if (extra != null) {
			setExtra(extra.getTitle());
		}

		setPeriod(issue.getPeriod());
		setLabel(issue.getLabel());
		setPage(ad.getPage());

		if (category != null) {
			setCategory(category.getName());
		}

		if (trademark != null) {
			setTrademark(trademark.getName());
		}

		if (brandname != null) {
			setBrandname(brandname.getName());
		}

		if (advertiser != null) {
			setAdvertiser(advertiser.getName());
		}

		if (support != null) {
			setSupportTrademark(support.getTrademark());
			setSupportBrandname(support.getBrandname());
			setSupportAdvertiser(support.getAdvertiser());
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMagazine() {
		return magazine;
	}

	public void setMagazine(String magazine) {
		this.magazine = magazine;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public Date getPeriod() {
		return period;
	}

	public void setPeriod(Date period) {
		this.period = period;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	public String getBrandname() {
		return brandname;
	}

	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}

	public String getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(String advertiser) {
		this.advertiser = advertiser;
	}

	public String getSupportTrademark() {
		return supportTrademark;
	}

	public void setSupportTrademark(String supportTrademark) {
		this.supportTrademark = supportTrademark;
	}

	public String getSupportBrandname() {
		return supportBrandname;
	}

	public void setSupportBrandname(String supportBrandname) {
		this.supportBrandname = supportBrandname;
	}

	public String getSupportAdvertiser() {
		return supportAdvertiser;
	}

	public void setSupportAdvertiser(String supportAdvertiser) {
		this.supportAdvertiser = supportAdvertiser;
	}
}
